//
//  AppDelegate.h
//  2048 game
//
//  Created by Jigs on 2/6/16.
//  Copyright © 2016 Jigar Panchal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

