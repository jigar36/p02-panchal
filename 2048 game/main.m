//
//  main.m
//  2048 game
//
//  Created by Jigs on 2/6/16.
//  Copyright © 2016 Jigar Panchal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
