//
//  ViewController.h
//  2048 game
//
//  Created by Jigs on 2/6/16.
//  Copyright © 2016 Jigar Panchal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *blocks;
@property (weak, nonatomic) IBOutlet UILabel *Score;
extern int score;
@property (weak, nonatomic) IBOutlet UIButton *PlayAgain;
@end

