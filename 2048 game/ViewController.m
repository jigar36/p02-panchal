//
//  ViewController.m
//  2048 game
//
//  Created by Jigs on 2/6/16.
//  Copyright © 2016 Jigar Panchal. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize blocks;

int score=0;

NSUserDefaults *defaults;

- (void)viewDidLoad {
    [super viewDidLoad];
    defaults = [NSUserDefaults standardUserDefaults];
    [defaults synchronize];
  
//Generating 1st random Number
    NSInteger n1 = arc4random_uniform(16);
    if([[[blocks objectAtIndex:n1] text] isEqual:@""])
    {
        [[blocks objectAtIndex:n1] setText: @"2"];
        [self TileBackground];
        
}
//generating second random number
    NSInteger n2 = arc4random_uniform(16);
    if(n2==n1)//checking if same random number is generated.
    {
        n2 = arc4random_uniform(16);
    }
    if([[[blocks objectAtIndex:n2] text] isEqual:@""])
    {
        [[blocks objectAtIndex:n2] setText: @"2"];
        [self TileBackground];
    }
 
    
    //guesture
    //left swipe
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    //right swipe
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(didSwipe:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    //swipe up
    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc]  initWithTarget:self action:@selector(didSwipe:)];
    swipeUp.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:swipeUp];
    //swipe down
    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeDown];
    // image set for play again button
    UIImage *buttonImage = [UIImage imageNamed:@"Play4.png"];
    [_PlayAgain setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [self.view addSubview:_PlayAgain];
  
}
-(void)didSwipe:(UISwipeGestureRecognizer*)swipe{
    //left swipe
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        NSLog(@"Swipe Left");
        int j=0;
while(j<=12) {
if([[[blocks objectAtIndex:j] text] isEqual:@""])
  {
      [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+1)] text]];
      [[blocks objectAtIndex:(j+1)] setText: @""];
  }
//block 4
if([[[blocks objectAtIndex:j+1] text] isEqual:@""])
{
    if([[[blocks objectAtIndex:j] text] isEqual:@""])
    {
        [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+2)] text]];
        [[blocks objectAtIndex:(j+2)] setText: @""];
    }
    else
    {
        [[blocks objectAtIndex:j+1] setText: [[blocks objectAtIndex:(j+2)] text]];
        [[blocks objectAtIndex:(j+2)] setText: @""];
    }
 }
//block 8
    if([[[blocks objectAtIndex:j+2] text] isEqual:@""])
    {
        if([[[blocks objectAtIndex:j] text] isEqual:@""])
        {
            [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+3)] text]];
            [[blocks objectAtIndex:(j+3)] setText: @""];
        }
        else if([[[blocks objectAtIndex:j+1] text] isEqual:@""])
        {
            [[blocks objectAtIndex:j+1] setText: [[blocks objectAtIndex:(j+3)] text]];
            [[blocks objectAtIndex:(j+3)] setText: @""];
        }
        else
        {
            [[blocks objectAtIndex:j+2] setText: [[blocks objectAtIndex:(j+3)] text]];
            [[blocks objectAtIndex:(j+3)] setText: @""];
        }
    }
    j=j+4;
}

     
int k = 0;
while(k<=12)
{
    if ([[[blocks objectAtIndex:k] text] isEqualToString:[[blocks objectAtIndex:(k+1)] text]]) {
    int m = [[[blocks objectAtIndex:k] text] intValue];
    int n = [[[blocks objectAtIndex:(k+1)] text] intValue];
    int res = m + n;
        if(res > 0)
        {
            score = score + res;
            [[blocks objectAtIndex:k] setText :[NSString stringWithFormat:@"%d",res] ];
            //reseting the 2nd block
            [[blocks objectAtIndex:(k+1)] setText:@""];
        }
    }
    if ([[[blocks objectAtIndex:(k+1)] text] isEqualToString:[[blocks objectAtIndex:(k+2)] text]]) {
        int m = [[[blocks objectAtIndex:(k+1)] text] intValue];
        int n = [[[blocks objectAtIndex:(k+2)] text] intValue];
        int res = m + n;
        if(res > 0)
        {
            score = score + res;
            [[blocks objectAtIndex:(k+1)] setText :[NSString stringWithFormat:@"%d",res] ];
            //reseting the 2nd block
            [[blocks objectAtIndex:(k+2)] setText:@""];
        }
    }
    if ([[[blocks objectAtIndex:(k+2)] text] isEqualToString:[[blocks objectAtIndex:(k+3)] text]]) {
        int m = [[[blocks objectAtIndex:(k+2)] text] intValue];
        int n = [[[blocks objectAtIndex:(k+3)] text] intValue];
        int res = m + n;
        if(res > 0)
        {
            score = score + res;
            [[blocks objectAtIndex:(k+2)] setText :[NSString stringWithFormat:@"%d",res] ];
            //resetting the 2nd block
            [[blocks objectAtIndex:(k+3)] setText:@""];
       }
    }
    k=k+4;
    _Score.text= [NSString stringWithFormat:@"%d", score];
    }
        j=0;
        while(j<=12) {
            //block 0
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+1)] text]];
                [[blocks objectAtIndex:(j+1)] setText: @""];
            }
            if([[[blocks objectAtIndex:j+1] text] isEqual:@""])
            {
                if([[[blocks objectAtIndex:j] text] isEqual:@""])
                {
                    [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+2)] text]];
                    [[blocks objectAtIndex:(j+2)] setText: @""];
                }
                else
                {
 
                    [[blocks objectAtIndex:j+1] setText: [[blocks objectAtIndex:(j+2)] text]];
                    [[blocks objectAtIndex:(j+2)] setText: @""];
                }
            }
            //block 8
            if([[[blocks objectAtIndex:j+2] text] isEqual:@""])
            {
                if([[[blocks objectAtIndex:j] text] isEqual:@""])
                {
                    [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+3)] text]];
                    [[blocks objectAtIndex:(j+3)] setText: @""];
                }
                else if([[[blocks objectAtIndex:j+1] text] isEqual:@""])
                {
                    [[blocks objectAtIndex:j+1] setText: [[blocks objectAtIndex:(j+3)] text]];
                    [[blocks objectAtIndex:(j+3)] setText: @""];
                }
                else
                {
                    [[blocks objectAtIndex:j+2] setText: [[blocks objectAtIndex:(j+3)] text]];
                    [[blocks objectAtIndex:(j+3)] setText: @""];
                }
            }
            j = j+4;
        }
        [self gameOver];
        [self gameWin];
        
        [self random];
        [self TileBackground];
        
        
        }
    // swipe right
    else if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
            NSLog(@"Swipe Right");
            int j=0;
            while(j<=12) {
                if([[[blocks objectAtIndex:(j+3)] text] isEqual:@""])
                {
                    [[blocks objectAtIndex:(j+3)] setText: [[blocks objectAtIndex:(j+2)] text]];
                    [[blocks objectAtIndex:(j+2)] setText: @""];
                }
                //block 4 check
                if([[[blocks objectAtIndex:(j+2)] text] isEqual:@""])
                {
                    if([[[blocks objectAtIndex:(j+3)] text] isEqual:@""])
                    {
                        [[blocks objectAtIndex:(j+3)] setText: [[blocks objectAtIndex:(j+1)] text]];
                        [[blocks objectAtIndex:(j+1)] setText: @""];
                    }
                    else
                    {
                        [[blocks objectAtIndex:(j+2)] setText: [[blocks objectAtIndex:(j+1)] text]];
                        [[blocks objectAtIndex:(j+1)] setText: @""];
                    }
                }
                //block 8 check
                if([[[blocks objectAtIndex:(j+1)] text] isEqual:@""])
                {
                    if([[[blocks objectAtIndex:j+3] text] isEqual:@""])
                    {
                        [[blocks objectAtIndex:(j+3)] setText: [[blocks objectAtIndex:(j)] text]];
                        [[blocks objectAtIndex:(j)] setText: @""];
                    }
                    else if([[[blocks objectAtIndex:j+2] text] isEqual:@""])
                    {
                        [[blocks objectAtIndex:(j+2)] setText: [[blocks objectAtIndex:(j)] text]];
                        [[blocks objectAtIndex:(j)] setText: @""];
                    }
                    else
                    {
                        [[blocks objectAtIndex:(j+1)] setText: [[blocks objectAtIndex:(j)] text]];
                        [[blocks objectAtIndex:(j)] setText: @""];
                    }
                }
                j=j+4;
            }
        
            int k = 0;
            while(k<=12)
            {
                if ([[[blocks objectAtIndex:(k+3)] text] isEqualToString:[[blocks objectAtIndex:(k+2)] text]]) {
                    int m = [[[blocks objectAtIndex:k+3] text] intValue];
                    int n = [[[blocks objectAtIndex:(k+2)] text] intValue];
                    int res = m + n;
                    if(res > 0)
                    {
                        score = score + res;
                        [[blocks objectAtIndex:k+3] setText :[NSString stringWithFormat:@"%d",res] ];
                        //resetting the 2nd block
                        [[blocks objectAtIndex:(k+2)] setText:@""];
                    }
                }
                if ([[[blocks objectAtIndex:(k+2)] text] isEqualToString:[[blocks objectAtIndex:(k+1)] text]]) {
                    int m = [[[blocks objectAtIndex:(k+2)] text] intValue];
                    int n = [[[blocks objectAtIndex:(k+1)] text] intValue];
                    int res = m + n;
                    if(res > 0)
                    {
                        [[blocks objectAtIndex:(k+2)] setText :[NSString stringWithFormat:@"%d",res] ];
                        //resetting the 2nd block
                        [[blocks objectAtIndex:(k+1)] setText:@""];
                    }
                }
                if ([[[blocks objectAtIndex:(k+1)] text] isEqualToString:[[blocks objectAtIndex:(k)] text]]) {
                    int m = [[[blocks objectAtIndex:(k+1)] text] intValue];
                    int n = [[[blocks objectAtIndex:(k)] text] intValue];
                    int res = m + n;
                    if(res > 0)
                    {
                        score = score + res;
                        [[blocks objectAtIndex:(k+1)] setText :[NSString stringWithFormat:@"%d",res] ];
                        //resetting the 2nd block
                        [[blocks objectAtIndex:(k)] setText:@""];
                    }
                }
                k=k+4;
                _Score.text= [NSString stringWithFormat:@"%d", score];

            }
            j=0;
            while(j<=12) {
                //check for 0 block
                if([[[blocks objectAtIndex:j+3] text] isEqual:@""])
                {
                    [[blocks objectAtIndex:j+3] setText: [[blocks objectAtIndex:(j+2)] text]];
                    [[blocks objectAtIndex:(j+2)] setText: @""];
                }
                //block 4 check
                if([[[blocks objectAtIndex:j+2] text] isEqual:@""])
                {
                    if([[[blocks objectAtIndex:j+3] text] isEqual:@""])
                    {
                        [[blocks objectAtIndex:j+3] setText: [[blocks objectAtIndex:(j+1)] text]];
                        [[blocks objectAtIndex:(j+1)] setText: @""];
                    }
                    else
                    {
                        [[blocks objectAtIndex:j+2] setText: [[blocks objectAtIndex:(j+1)] text]];
                        [[blocks objectAtIndex:(j+1)] setText: @""];
                    }
                }
                //block 8th check
                if([[[blocks objectAtIndex:j+1] text] isEqual:@""])
                {
                    if([[[blocks objectAtIndex:j+3] text] isEqual:@""])
                    {
                        [[blocks objectAtIndex:j+3] setText: [[blocks objectAtIndex:(j)] text]];
                        [[blocks objectAtIndex:(j)] setText: @""];
                    }
                    else if([[[blocks objectAtIndex:j+2] text] isEqual:@""])
                    {
                        [[blocks objectAtIndex:j+2] setText: [[blocks objectAtIndex:(j)] text]];
                        [[blocks objectAtIndex:(j)] setText: @""];
                    }
                    else
                    {
                        [[blocks objectAtIndex:j+1] setText: [[blocks objectAtIndex:(j)] text]];
                        [[blocks objectAtIndex:(j)] setText: @""];
                    }
                }
                j = j+4;
            }
            [self gameOver];
            [self gameWin];
        
            [self random];
            [self TileBackground];
        }
    //swipe up
    else if (swipe.direction == UISwipeGestureRecognizerDirectionUp) {
            NSLog(@"Swipe Up");
            for (int j=0; j<=3; j++) {
            //block 0 check
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+4)] text]];
                [[blocks objectAtIndex:(j+4)] setText: @""];
            }
            //block 4 check
            if([[[blocks objectAtIndex:j+4] text] isEqual:@""])
            {
                if([[[blocks objectAtIndex:j] text] isEqual:@""])
                {
                    [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+8)] text]];
                    [[blocks objectAtIndex:(j+8)] setText: @""];
                }
                else
                {
                    [[blocks objectAtIndex:j+4] setText: [[blocks objectAtIndex:(j+8)] text]];
                    [[blocks objectAtIndex:(j+8)] setText: @""];
                }
            }
            
            //block 8 check
            if([[[blocks objectAtIndex:j+8] text] isEqual:@""])
            {
                if([[[blocks objectAtIndex:j] text] isEqual:@""])
                {
                    [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+12)] text]];
                    [[blocks objectAtIndex:(j+12)] setText: @""];
                }
                else if([[[blocks objectAtIndex:j+4] text] isEqual:@""])
                {
                    [[blocks objectAtIndex:j+4] setText: [[blocks objectAtIndex:(j+12)] text]];
                    [[blocks objectAtIndex:(j+12)] setText: @""];
                }
                else
                {
                    [[blocks objectAtIndex:j+8] setText: [[blocks objectAtIndex:(j+12)] text]];
                    [[blocks objectAtIndex:(j+12)] setText: @""];
                }
            }
        }
     
        int k = 0;
        while(k<=11)
        {
            if ([[[blocks objectAtIndex:k] text] isEqualToString:[[blocks objectAtIndex:(k+4)] text]]) {
                int m = [[[blocks objectAtIndex:k] text] intValue];
                int n = [[[blocks objectAtIndex:(k+4)] text] intValue];
                int res = m + n;
                if(res > 0)
                {
                    score = score + res;
                    [[blocks objectAtIndex:k] setText :[NSString stringWithFormat:@"%d",res] ];
                    //resetting the 2nd block
                    [[blocks objectAtIndex:(k+4)] setText:@""];
                }
            }
            k++;
           _Score.text= [NSString stringWithFormat:@"%d", score];
           
        }
        for (int j=0; j<=3; j++) {
            //block 0 check
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+4)] text]];
                [[blocks objectAtIndex:(j+4)] setText: @""];
            }
            //block 4 check
            if([[[blocks objectAtIndex:j+4] text] isEqual:@""])
            {
                if([[[blocks objectAtIndex:j] text] isEqual:@""])
                {
                    [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+8)] text]];
                    [[blocks objectAtIndex:(j+8)] setText: @""];

                }
                else
                {
                    [[blocks objectAtIndex:j+4] setText: [[blocks objectAtIndex:(j+8)] text]];
                    [[blocks objectAtIndex:(j+8)] setText: @""];
                }
            }

            //block 8 check
            if([[[blocks objectAtIndex:j+8] text] isEqual:@""])
            {
                if([[[blocks objectAtIndex:j] text] isEqual:@""])
                {
                    [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+12)] text]];
                    [[blocks objectAtIndex:(j+12)] setText: @""];

                }
                else if([[[blocks objectAtIndex:j+4] text] isEqual:@""])
                {
                    [[blocks objectAtIndex:j+4] setText: [[blocks objectAtIndex:(j+12)] text]];
                    [[blocks objectAtIndex:(j+12)] setText: @""];
                    }
                else
                {
                    [[blocks objectAtIndex:j+8] setText: [[blocks objectAtIndex:(j+12)] text]];
                    [[blocks objectAtIndex:(j+12)] setText: @""];
                }
            }
        }
            [self gameOver];
            [self gameWin];
        
            [self random];
            [self TileBackground];
        }
    //swipe down
    else if (swipe.direction == UISwipeGestureRecognizerDirectionDown) {
            NSLog(@"Swipe Down");
            for(int dwn = 0;dwn < 4 ; dwn++)
            {
              
                if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""])
                {
                    [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn+8)] text]];
                    [[blocks objectAtIndex:(dwn+8)] setText:@""];
                }

                if([[[blocks objectAtIndex:(dwn+8)] text] isEqual:@""])
                {
                    if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""])
                    {
                        [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn+4)] text]];
                        [[blocks objectAtIndex:(dwn+4)] setText:@""];
                    }
                    else
                    {
                        [[blocks objectAtIndex:(dwn+8)] setText:[[blocks objectAtIndex:(dwn+4)] text]];
                        [[blocks objectAtIndex:(dwn+4)] setText:@""];
                    }
                }
                if([[[blocks objectAtIndex:(dwn+4)] text] isEqual:@""])
                {
                    if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""]) {
                        [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn)] text]];
                        [[blocks objectAtIndex:(dwn)] setText:@""];

                    }
                    else if ([[[blocks objectAtIndex:(dwn+8)] text] isEqual:@""])
                    {
                        [[blocks objectAtIndex:(dwn+8)] setText:[[blocks objectAtIndex:(dwn)] text]];
                        [[blocks objectAtIndex:(dwn)] setText:@""];

                    }
                    else
                    {
                        [[blocks objectAtIndex:(dwn+4)] setText:[[blocks objectAtIndex:(dwn)] text]];
                        [[blocks objectAtIndex:(dwn)] setText:@""];

                    }
                }
            }


            //addition for the same values of the blocks
            int k = 15;
            while(k>=4)
            {
                if ([[[blocks objectAtIndex:k] text] isEqualToString:[[blocks objectAtIndex:(k-4)] text]])
                {
                    int m = [[[blocks objectAtIndex:k] text] intValue];
                    int n = [[[blocks objectAtIndex:(k-4)] text] intValue];
                    int res = m + n;

                    if(res > 0)
                    {
                        score = score + res;

                        [[blocks objectAtIndex:k] setText :[NSString stringWithFormat:@"%d",res] ];

                        // the 2nd tile
                        [[blocks objectAtIndex:(k-4)] setText:@""];

                    }
                }
                k--;
                _Score.text= [NSString stringWithFormat:@"%d", score];

            }


            //blocks pushing downwards
            for(int dwn = 0;dwn < 4 ; dwn++)
            {
              
                if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""]) {
                    [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn+8)] text]];
                    [[blocks objectAtIndex:(dwn+8)] setText:@""];

                }
              
                if([[[blocks objectAtIndex:(dwn+8)] text] isEqual:@""])
                {
                    if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""]) {
                            [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn+4)] text]];
                        [[blocks objectAtIndex:(dwn+4)] setText:@""];

                    }
                    else
                    {
                        [[blocks objectAtIndex:(dwn+8)] setText:[[blocks objectAtIndex:(dwn+4)] text]];
                        [[blocks objectAtIndex:(dwn+4)] setText:@""];

                    }
                }
                
                if([[[blocks objectAtIndex:(dwn+4)] text] isEqual:@""])
                {
                    if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""]) {
                        [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn)] text]];
                        [[blocks objectAtIndex:(dwn)] setText:@""];
                    }
                    else if ([[[blocks objectAtIndex:(dwn+8)] text] isEqual:@""])
                    {
                        [[blocks objectAtIndex:(dwn+8)] setText:[[blocks objectAtIndex:(dwn)] text]];
                        [[blocks objectAtIndex:(dwn)] setText:@""];
                    }
                    else
                    {
                        [[blocks objectAtIndex:(dwn+4)] setText:[[blocks objectAtIndex:(dwn)] text]];
                        [[blocks objectAtIndex:(dwn)] setText:@""];

                    }
                }
            }
            [self gameOver];
            [self gameWin];
        
            [self random];
            [self TileBackground];

        }
    }


- (void)gameOver{
    
    int count = 0;
    Boolean game_flg = false;;
    //checking for gammeOver
    for (int game=0; game <= 11; game++)
    {
        count++;
        if([[[blocks objectAtIndex:game] text] isEqual:@""])
        {
            game_flg = true;
        }
        else
        {
            if((count%4) == 0)
            {
                if([[[blocks objectAtIndex:game] text] isEqualToString:[[blocks objectAtIndex:(game+4)] text]])
                {
                    game_flg = true;
                }
            }
            else
            {
                if([[[blocks objectAtIndex:game] text] isEqualToString:[[blocks objectAtIndex:(game+1)] text]])
                {
                    game_flg = true;
                }
                else if([[[blocks objectAtIndex:game] text] isEqualToString:[[blocks objectAtIndex:(game+4)] text]])
                {
                    game_flg = true;
                }
            }
            
            if(game == 11)
            {
                if([[[blocks objectAtIndex:12] text] isEqualToString:[[blocks objectAtIndex:13] text]])
                {
                    game_flg = true;
                }
                else if([[[blocks objectAtIndex:13] text] isEqualToString:[[blocks objectAtIndex:14] text]])
                {
                    game_flg = true;
                }
                else if([[[blocks objectAtIndex:14] text] isEqualToString:[[blocks objectAtIndex:15] text]])
                {
                    game_flg = true;
                }
            }
        }
        
        if(game_flg)
            break;
    }
    if(game_flg == false)
    {
        //alert pop up
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Game Over"
                                  message:@"Try Again"
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  otherButtonTitles:@"OK", nil];
        
        [alertView show];
    }
    
}

-(void)gameWin{
    
    for(int k=0; k<=15; k++)
    {
        if([[[blocks objectAtIndex:k] text] isEqual:@"2048"])
        {
            //alert pop up
            UIAlertView *alertView1 = [[UIAlertView alloc]
                                      initWithTitle:@"Game Win"
                                      message:@"Yes you did it...!"
                                      delegate:self
                                      cancelButtonTitle:@"Cancel"
                                      otherButtonTitles:@"OK", nil];
            
            [alertView1 show];
        }
    }
    
    
}

- (void)didReceiveMemoryWarning {
[super didReceiveMemoryWarning];
// Dispose of any resources that can be recreated.
}

-(void)random{
NSInteger num1 = arc4random_uniform(16);
if(!([[[blocks objectAtIndex:num1] text] isEqual:@""]))
{
    num1 = arc4random_uniform(16);
}
if([[[blocks objectAtIndex:num1] text] isEqual:@""])
    {
        [[blocks objectAtIndex:num1] setText: @"2"];
    }
}
- (IBAction)PlayAgain:(id)sender {
    
    for(int i=0; i<=15; i++)
    {
        [[blocks objectAtIndex:i] setText: @""];
        
        
       
    }
    _Score.text = @"0";
    NSInteger n1 = arc4random_uniform(16);
    if([[[blocks objectAtIndex:n1] text] isEqual:@""])
    {
        [[blocks objectAtIndex:n1] setText: @"2"];
    }
    // For generating second Random Number
    NSInteger n2 = arc4random_uniform(16);
    if(n2==n1)// to check if same random number is generated for the 2nd block as well.
    {
        n2 = arc4random_uniform(16);
    }
    if([[[blocks objectAtIndex:n2] text] isEqual:@""])
    {
        [[blocks objectAtIndex:n2] setText: @"2"];
    }
    [self TileBackground];
}

- (void)TileBackground{
    for(int j=0; j<=15; j++)
    {
        if ([[[blocks objectAtIndex:j] text] isEqual:@"2"]) {
            [[blocks objectAtIndex:j] setBackgroundColor:[UIColor colorWithRed:20.0f/255.0f
                                                                         green:20.0f/255.0f
                                                                        blue:80.0f/255.0f
                                                                         alpha:1.0f]];
        }
        else if (([[[blocks objectAtIndex:j] text] isEqual:@"4"])){
            [[blocks objectAtIndex:j] setBackgroundColor:[UIColor colorWithRed:20.0f/255.0f
                                                                         green:20.0f/255.0f
                                                                          blue:140.0f/255.0f
                                                                         alpha:1.0f]];
        }
        else if (([[[blocks objectAtIndex:j] text] isEqual:@"8"])){
            [[blocks objectAtIndex:j] setBackgroundColor:[UIColor colorWithRed:20.0f/255.0f
                                                                         green:60.0f/255.0f
                                                                          blue:220.0f/255.0f
                                                                         alpha:1.0f]];
        }
        else if (([[[blocks objectAtIndex:j] text] isEqual:@"16"])){
            [[blocks objectAtIndex:j] setBackgroundColor:[UIColor colorWithRed:20.0f/255.0f
                                                                         green:120.0f/255.0f
                                                                          blue:120.0f/255.0f
                                                                         alpha:1.0f]];
        }
        else if (([[[blocks objectAtIndex:j] text] isEqual:@"32"])){
            [[blocks objectAtIndex:j] setBackgroundColor:[UIColor colorWithRed:20.0f/255.0f
                                                                         green:160.0f/255.0f
                                                                          blue:120.0f/255.0f
                                                                         alpha:1.0f]];
        }
        else if (([[[blocks objectAtIndex:j] text] isEqual:@"64"])){
            [[blocks objectAtIndex:j] setBackgroundColor:[UIColor colorWithRed:20.0f/255.0f
                                                                         green:160.0f/255.0f
                                                                          blue:60.0f/255.0f
                                                                         alpha:1.0f]];
        }
        else if (([[[blocks objectAtIndex:j] text] isEqual:@"128"])){
            [[blocks objectAtIndex:j] setBackgroundColor:[UIColor colorWithRed:50.0f/255.0f
                                                                         green:160.0f/255.0f
                                                                          blue:60.0f/255.0f
                                                                         alpha:1.0f]];
        }
        else if (([[[blocks objectAtIndex:j] text] isEqual:@"256"])){
            [[blocks objectAtIndex:j] setBackgroundColor:[UIColor colorWithRed:80.0f/255.0f
                                                                         green:120.0f/255.0f
                                                                          blue:60.0f/255.0f
                                                                         alpha:1.0f]];
        }
        else if (([[[blocks objectAtIndex:j] text] isEqual:@"512"])){
            [[blocks objectAtIndex:j] setBackgroundColor:[UIColor colorWithRed:140.0f/255.0f
                                                                         green:70.0f/255.0f
                                                                          blue:60.0f/255.0f
                                                                         alpha:1.0f]];
        }
        else if (([[[blocks objectAtIndex:j] text] isEqual:@"1024"])){
            [[blocks objectAtIndex:j] setBackgroundColor:[UIColor colorWithRed:170.0f/255.0f
                                                                         green:30.0f/255.0f
                                                                          blue:60.0f/255.0f
                                                                         alpha:1.0f]];
        }
        else if (([[[blocks objectAtIndex:j] text] isEqual:@"2048"])){
            [[blocks objectAtIndex:j] setBackgroundColor:[UIColor colorWithRed:220.0f/255.0f
                                                                         green:30.0f/255.0f
                                                                          blue:30.0f/255.0f
                                                                         alpha:1.0f]];
        }
        else if (([[[blocks objectAtIndex:j] text] isEqual:@""])){
            [[blocks objectAtIndex:j] setBackgroundColor:[UIColor colorWithRed:154.0f/255.0f
                                                                         green:154.0f/255.0f
                                                                          blue:154.0f/255.0f
                                                                         alpha:1.0f]];
        }
    }
}
@end