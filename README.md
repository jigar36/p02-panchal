# README #

It's an clone of the famous game 2048.
2048 is a single-player sliding block puzzle game.
The game objective is to slide numbered tiles on a grid to combine them and create a tile with the number 2048.

![Simulator Screen Shot Mar 27, 2016, 5.36.41 PM.png](https://bitbucket.org/repo/xor8zy/images/1405346791-Simulator%20Screen%20Shot%20Mar%2027,%202016,%205.36.41%20PM.png)![Simulator Screen Shot Mar 27, 2016, 5.36.50 PM.png](https://bitbucket.org/repo/xor8zy/images/3370990907-Simulator%20Screen%20Shot%20Mar%2027,%202016,%205.36.50%20PM.png)![Simulator Screen Shot Mar 27, 2016, 5.42.49 PM.png](https://bitbucket.org/repo/xor8zy/images/289765347-Simulator%20Screen%20Shot%20Mar%2027,%202016,%205.42.49%20PM.png)